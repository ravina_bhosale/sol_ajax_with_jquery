﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Sol_AjaxWithJQuery.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

     <style type="text/css">

         .divStyle{
            width:50%;
            height:40px;
            line-height:40px;
            background-color:greenyellow;
            border:solid;
            border-color:black;
            text-align:center;
            vertical-align:central;
            align-content:stretch;
            display:none;
        }

         .btnSyle{
             background-color:burlywood;
             color:green;
             font-size:large;
             width:80px;
         }

    </style>

    <script type="text/javascript" src="Scripts/jquery-3.1.1.min.js"></script>

    <script type="text/javascript">

        // use function pageLoad if you are using Ajax to run Jquery
        function pageLoad()
        {
            $(document).ready(function () {

                 $("#<%=btnSubmit.ClientID%>")
                .on("click", function () {

               $("#<%=divi.ClientID%>")
                   .slideToggle(1000);
       

                });

            });

        }

            </script>

       
</head>
<body>
    <form id="form1" runat="server">
    <div>

         <asp:ScriptManager ID="scriptManager" runat="server">
            </asp:ScriptManager>

            <asp:UpdatePanel ID="updatePanel" runat="server">
                <ContentTemplate>

                <div id="divi" runat ="server" class="divStyle"></div>
                
                 <asp:Button ID="btnSubmit" Text="Click" runat="server" CssClass="btnSyle" OnClick="btnSubmit_Click" />
                   
                </ContentTemplate>
            </asp:UpdatePanel>

        </div>
    </form>
</body>
</html>
